<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muistio extends CI_Controller {
        public function __construct() {
            parent::__construct();
            $this->load->model('muistio_model');
            $this->load->library('util');
            $this->load->library('form_validation');
        }

        public function index($asiakas_id) {
            $this->session->set_userdata('asiakas_id',$asiakas_id); /* asiakas-id istuntomuuttujassa */
            $data['aika'] = date('d.m.Y H.i', time());
            $data['main_content'] = 'muistio_view'; /* tämä hakee sivupohjan */
            $this->load->view('template',$data);
            $data['muistiinpanot'] = $this->muistio_model->hae_kaikki();
        }
        
        public function tallenna() {
            $data = array(
                'tallennettu' => $this->util->format_fin_to_sqldate($this->input->post('aika')),
                'teksti' => $this->input->post('teksti'),
                'asiakas_id' => $this->session->userdata('asiakas_id')
            );
            
            $this->muistio_model->lisaa($data);
            redirect('muistio/index/' . $this->session->userdata('asiakas_id'));
            
        }
        
        public function poista($id) {
            $this->model_model->poista(intval($id));
            redirect('asiakas/index');
        }
}