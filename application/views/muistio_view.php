<div class="row"> 
    <div class="col-xs-12 col-md-offset-4 col-md-4"> <!-- Bootstrappia, xs tarkottaa että kaikilla näytöillä -->
        <h3>Toimet - Testiyritys</h3>
        <?php print validation_errors();?>
        <form role="form" method="post" action="<?php print site_url(). 'muistio/tallenna'?>">
            <div class="form-group">
                <label for="aika">Aika:</label>
                <input type="datetime" name="aika" value="<?php print($aika);?>" class="form-control">
            </div>
            <div class="form-group">
                <label for="teksti">Teksti:</label>
                <input type="text" name="teksti" class="form-control">
            </div>
            <a class="btn btn-default" href="<?php print site_url() . 'asiakas/index'?>">
                Etusivulle
            </a>
            <button class="btn btn-primary">Tallenna</button>
        </form>
    </div>
    <?php
    foreach ($muistiinpanot as $muistio) {
        print "<tr>";
        print "<td>$asiakas->id</td>";
        print "<td>$asiakas->sukunimi</td>";
        print "<td>$asiakas->etunimi</td>";
        print "<td>$asiakas->lahiosoite</td>";
        print "<td>$asiakas->postitoimipaikka</td>";
        print "<td>$asiakas->postinumero</td>";
        print "<td>" . anchor("asiakas/poista/$asiakas->id","Poista") . "</td>";
        print "<td>" . anchor("asiakas/muokkaa/$asiakas->id","Muokkaa") . "</td>";
        print "<td>" . anchor("muistio/index/$asiakas->id","Muistio") . "</td>";
        print "</tr>";
    }
    ?>
</div>